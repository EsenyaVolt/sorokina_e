#pragma once
#include "Individual.h"
#include <iostream>

using namespace ga;
using namespace std;
using namespace ANN;

Individual::Individual()
{
}

Individual::~Individual()
{
}

shared_ptr<IIndividual> Individual::Mutation()
{
	float min = 0.000001;
	float max = 0.2;
	float help = 0.0;
	int randInd = (rand()) / RAND_MAX * (2 - 0) + 0;

	for (unsigned int from_index = 0; from_index < pAnn->weights[randInd].size(); from_index++)
	{
		for (unsigned int to_index = 0; to_index < pAnn->weights[randInd][from_index].size(); to_index++)
		{
			help = (float)(rand()) / RAND_MAX * (max - min) + min;
			pAnn->weights[randInd][from_index][to_index] -= help * (pAnn->weights[randInd][from_index][to_index]);
		}
	}

	shared_ptr<Individual> mutation = make_shared<Individual>();
	mutation->pAnn = pAnn;
	return mutation;
}

shared_ptr<IIndividual> Individual::Crossover(shared_ptr<IIndividual> individual)
{
	shared_ptr<Individual> individ2 = make_shared<Individual>();
	individ2 = dynamic_pointer_cast<Individual>(individual);

	std::shared_ptr<ga::Individual> individ_new = make_shared<Individual>();
	individ_new->pAnn = pAnn;

	int randInd = (rand()) / RAND_MAX * (2 - 0) + 0;

	for (unsigned int from_index = 0; from_index < pAnn->weights[randInd].size(); from_index++)
	{
		for (unsigned int to_index = 0; to_index < pAnn->weights[randInd][from_index].size(); to_index++)
		{
			individ_new->pAnn->weights[randInd][from_index][to_index] = individ2->pAnn->weights[randInd][from_index][to_index];
		}
	}

	return individ_new;
}

pair<int, int> Individual::Spare(shared_ptr<IIndividual> individual)
{
	vector<vector<float>> input;
	vector<vector<float>> output;

	//������ �������� ������ �� ����� LearningData.txt
	LoadData("../ANNTrainer/ANNTrainerInputData.txt", input, output);

	vector<vector<float>> predict1;
	vector<vector<float>> predict2;

	shared_ptr<Individual> individ2 = make_shared<Individual>();
	individ2 = dynamic_pointer_cast<Individual>(individual);

	//������������� ����� ���� �� ��� ����� (������ ���� �� ���� �������� ������)
	for (int i = 0; i < input.size(); i++)
	{
		predict1.push_back(pAnn->Predict(input[i]));
		predict2.push_back(individ2->pAnn->Predict(input[i]));
	}

	int Score1 = 0;
	int Score2 = 0;

	// ������� ������ ����� ���������� ������� � �������� ������� ����
	for (int i = 0; i < predict1.size(); i++)
	{
		for (int j = 0; j < predict1[i].size(); j++)
		{
			float help1 = fabs(output[i][j] - predict1[i][j]);
			error += help1;

			float help2 = fabs(output[i][j] - predict2[i][j]);
			individ2->error += help2;

			if (help1 > eps)
			{
				Score1 += negative;
			}

			if (help2 > eps)
			{
				Score2 += negative;
			}
		}
	}

	error /= input.size();
	individ2->error /= input.size();

	if (error < individ2->error)
	{
		Score1 += positive;
		Score2 += negative;
	}
	else
	{
		Score2 += positive;
		Score1 += negative;
	}

	pair<int, int> Result;
	Result.first = Score1;
	Result.second = Score2;

	return Result;
}

vector<float> Individual::MakeDecision(vector<float>& input)
{
	return vector<float>();
}

shared_ptr<IIndividual> Individual::Clone()
{
	return shared_ptr<IIndividual>();
}

pIIndividual Individual::CreateIndividual()
{
	pAnn->RandomInit();
	shared_ptr<IIndividual> random_individual = dynamic_pointer_cast<IIndividual>(pAnn);
	pIIndividual pInd = random_individual;
	return pInd;
}
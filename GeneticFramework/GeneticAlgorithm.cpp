#include <algorithm>
#include "GeneticAlgorithm.h"

using namespace ga;
using namespace std;

ga::GeneticAlgorithm::GeneticAlgorithm()
{
}


ga::GeneticAlgorithm::~GeneticAlgorithm()
{
}

ga::pEpoch ga::GeneticAlgorithm::Selection(double unchange_perc, double mutation_perc, double crossover_perc)
{
	// ���������� ������ �� ��������� �����.
	sort(epoch->population.begin(), epoch->population.end(),
		[](std::pair<int, pIIndividual> a, std::pair<int, pIIndividual> b)
		{
			return a.first > b.first;
		});

	// ����������� �����
	Epoch* newEpoch = new Epoch();
	pEpoch pNewEpoch(newEpoch);
	pNewEpoch->population.resize(epoch->population.size());

	int SizeUnchange = epoch->population.size() * (unchange_perc / 100.);
	int SizeMutation = SizeUnchange * (mutation_perc / 100.);
	int SizeCrossover = SizeUnchange * (crossover_perc / 100.);

	// ������������� ������ 
	for (int i = 0; i < SizeUnchange; i++)
	{
		pNewEpoch->population[i].first = 0;
		pNewEpoch->population[i].second = epoch->population[i].second;
	}

	int index = 0;
	pIIndividual pInd;
	// ��������� ����� ��������� �������, ����������� ������������ ������ ������ ���������� �����
	for (int i = 0; i < SizeCrossover; i++)
	{
		while (index == i)
		{
			index = (rand()) / RAND_MAX * ((epoch->population.size() - 1) - 0) + 0;

			if (index != i)
			{
				pInd = epoch->population[index].second->Crossover(epoch->population[i].second);
			}
		}

		pNewEpoch->population[SizeUnchange + i].first = 0;
		pNewEpoch->population[SizeUnchange + i].second = pInd;
	}

	// ��������� ����� ��������� �������, ����������� �������� ������ ������ ���������� �����
	for (int i = SizeUnchange + SizeCrossover; i < epoch->population.size(); i++)
	{
		index = (rand()) / RAND_MAX * ((epoch->population.size() - 1) - 0) + 0;
		pInd = epoch->population[index].second->Mutation();

		pNewEpoch->population[i].first = 0;
		pNewEpoch->population[i].second = pInd;
	}

	return pNewEpoch;
}


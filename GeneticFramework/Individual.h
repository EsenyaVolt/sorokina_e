#pragma once

#include "IIndividual.h"
#include "../ANNLib/Heir.h"

using namespace ANN;
using namespace std;

namespace ga
{
	class Individual : public IIndividual, public ANeuralNetworkHeir
	{
	protected:
		// ������ / ������
		int positive = 10;
		int negative = -10;
	public:
		Individual();
		~Individual();

		shared_ptr<ANeuralNetwork> pAnn;
		float error = 0.0;
		float eps = 0.001;

		/**
		 * ��������� ������� �����.
		 * @return ������������ �����.
		 */
		std::shared_ptr<IIndividual> Mutation();

		/**
		 * ��������� ����������� ������� ����� � ������ ������.
		 * @param individual - ����� � ������� ����� ��������� �����������.
		 * @return �������� ����� ����� �����������.
		 */
		std::shared_ptr<IIndividual> Crossover(std::shared_ptr<IIndividual> individual);

		/**
		 * �������� ������������ ����� ������� � ������ ������.
		 * @param individual - ������ �����.
		 * @return ���� ����. ������ �������� - ���������� ����� ��������� ������� ������.
		 *					  ������ �������� - ���������� �����, ��������� ������ ������.
		 */
		std::pair<int, int> Spare(std::shared_ptr<IIndividual> individual);

		/**
		 * ������� �������.
		 * � �������� ������������ ����� ���������� ��������� �������,
		 * �� ����� ������� ������� �������� ������������.
		 * @param input - ������� ������
		 * @return �������� ������.
		 */
		std::vector<float> MakeDecision(std::vector<float>& input);

		/**
		 * ����������� ������� �����.
		 * @return ����� ������� �����.
		 */
		std::shared_ptr<IIndividual> Clone();

		pIIndividual CreateIndividual();
	};
	// ��������������� ���� "����� ��������� �� �������".
	typedef std::shared_ptr<Individual> pIndividual;
};
#pragma once
#include <iostream>
#include <algorithm>
#include <locale.h>
#include <iomanip>
#include "Individual.h"
#include "GeneticAlgorithm.h"

using  namespace std;
using namespace ga;

int main()
{
	setlocale(LC_ALL, "ru");
	// Hyperparameters
	int epoch_size = 1000;
	int ann_size = 100;
	double unchange_perc = 50;
	double mutation_perc = 50;
	double crossover_perc = 50;

	Epoch* Epoch_tek = new Epoch();
	pEpoch pEpoch_tek(Epoch_tek);
	pEpoch_tek->population.resize(ann_size);

	// �������������� ������, ������������� ������������ ��������
	GeneticAlgorithm* genAlg = new GeneticAlgorithm();

	// ������� ��������� ���������
	for (int i = 0; i < ann_size; i++)
	{
		vector<size_t> config;
		config.push_back(2);
		config.push_back(60);
		config.push_back(60);
		config.push_back(1);

		ANeuralNetwork::ActivationType type = ANeuralNetwork::ActivationType::POSITIVE_SYGMOID;
		float scale = 1.0;

		auto nn = CreateNeuralNetwork(config, type, scale);
		nn->RandomInit();

		// �������������� ������-�������
		Individual* Indvdl = new Individual();
		Indvdl->pAnn = nn;
		pIndividual pInd(Indvdl);

		pEpoch_tek->population[i].first = 0;
		pEpoch_tek->population[i].second = pInd;
	}

	vector<vector<float>> input;
	vector<vector<float>> output;

	//������ �������� ������ �� ����� LearningData.txt
	LoadData("../ANNTrainer/ANNTrainerInputData.txt", input, output);

	for (int i = 0; i < epoch_size; i++)
	{
		pEpoch_tek->EpochBattle();
		genAlg->epoch = pEpoch_tek;

		Epoch* newEpoch = new Epoch();
		pEpoch pNextEpoch(newEpoch);
		pNextEpoch = genAlg->Selection(unchange_perc, mutation_perc, crossover_perc);

		std::shared_ptr<ga::Individual> nn = make_shared<Individual>();
		nn = dynamic_pointer_cast<Individual>(pEpoch_tek->population[0].second);

		cout << "Epoch: " << i + 1 << "\tError: " << nn->error;
		pEpoch_tek = pNextEpoch;

		vector<vector<float>> predictOut;

		std::shared_ptr<ga::Individual> ANN = make_shared<Individual>();
		ANN = dynamic_pointer_cast<Individual>(pEpoch_tek->population[0].second);

		//������������� ����� ���� �� ��� ����� (������ ���� �� ���� �������� ������)
		for (int k = 0; k < input.size(); k++)
		{
			predictOut.push_back(ANN->pAnn->Predict(input[k]));
		}

		//������� ����������
		cout << "\n������� ������\t\t�������� ������\t\t������������� �����\n\n";
		for (int k = 0; k < input.size(); k++)
		{
			for (int j = 0; j < input[k].size(); j++)
			{
				cout << input[k][j] << "\t";
			}
			cout << "\t       ";
			for (int j = 0; j < output[k].size(); j++)
			{
				cout << output[k][j] << "\t";
			}
			cout << "\t       ";
			for (int j = 0; j < predictOut[k].size(); j++)
			{
				cout << predictOut[k][j] << "\t\t";
			}
			cout << "\n";
		}
		cout << "\n\n";
	}

	return 0;
}
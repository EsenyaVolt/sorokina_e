#pragma once
#define ANNDLL_EXPORTS
#include <iostream>
#include <Heir.h>

using namespace std;
using namespace ANN;

shared_ptr<ANN::ANeuralNetwork> ANN::CreateNeuralNetwork(vector<size_t>& configuration,
	ANeuralNetwork::ActivationType activation_type, float scale)
{
	return make_shared<ANeuralNetworkHeir>(configuration, activation_type, scale);
}

ANeuralNetworkHeir::ANeuralNetworkHeir()
{
	configuration.clear();
	configuration.push_back(2);
	configuration.push_back(10);
	configuration.push_back(10);
	configuration.push_back(1);
	activation_type = ANN::ANeuralNetwork::ActivationType::POSITIVE_SYGMOID;
	scale = 1.0;
}

ANeuralNetworkHeir::~ANeuralNetworkHeir()
{
}

ANeuralNetworkHeir::ANeuralNetworkHeir(vector<size_t>& config, ANeuralNetwork::ActivationType activation_t, float scl)
{
	configuration.clear();
	for (int i = 0; i < config.size(); i++) {
		configuration.push_back(config[i]);
	}

	activation_type = activation_t;

	scale = scl;
}

//��������� ������ �������� ����
string ANeuralNetworkHeir::GetType()
{
	string str = "��� ��������� ����: ������������ ����������, ����� ����������: �������� �����";
	return str;
}

//��������������� ������ �� ���������� �����
vector<float> ANeuralNetworkHeir::Predict(vector<float>& input)
{
	vector<float> Out;
	ValueOfActivFuncForEveryNeuron.clear();
	ValueOfActivFuncForEveryNeuron.resize(configuration.size());

	for (unsigned int layer_index = 0; layer_index < configuration.size(); layer_index++) {
		ValueOfActivFuncForEveryNeuron[layer_index].resize(configuration[layer_index]);
	}

	for (unsigned int from_index = 0; from_index < input.size(); from_index++) {

		ValueOfActivFuncForEveryNeuron[0][from_index] = input[from_index];
	}

	for (unsigned int layer_index = 0; layer_index < configuration.size() - 1; layer_index++) {
		for (unsigned int from_index = 0; from_index < weights[layer_index].size(); from_index++) {
			for (unsigned int to_index = 0; to_index < weights[layer_index][from_index].size(); to_index++) {

				ValueOfActivFuncForEveryNeuron[layer_index + 1][to_index] += ValueOfActivFuncForEveryNeuron[layer_index][from_index]
					* weights[layer_index][from_index][to_index];
			}
		}

		for (int i = 0; i < ValueOfActivFuncForEveryNeuron[layer_index + 1].size(); i++) {
			ValueOfActivFuncForEveryNeuron[layer_index + 1][i] = Activation(ValueOfActivFuncForEveryNeuron[layer_index + 1][i]);
		}
	}

	for (int i = 0; i < configuration[configuration.size() - 1]; i++) {
		Out.push_back(ValueOfActivFuncForEveryNeuron[configuration.size() - 1][i]);
	}

	return Out;
	Out.clear();
}

float ANN::BackPropTraining(std::shared_ptr<ANN::ANeuralNetwork> ann, std::vector<std::vector<float>>& inputs, std::vector<std::vector<float>>& outputs, int maxIters, float eps, float speed, bool std_dump)
{
	int countOfIteration = 0;
	float error = 0;

	while (countOfIteration <= maxIters)
	{
		error = 0;
		for (int i = 0; i < inputs.size(); i++)
		{
			error += BackPropTrainingIteration(ann, inputs[i], outputs[i], speed);
		}

		error /= inputs.size();

		countOfIteration++;

		if (std_dump == true && countOfIteration % 100 == 0)
			std::cout << "Iteration: " << countOfIteration << "\t\tError: " << (error) << std::endl;

		if (error <= eps)
			break;

	}

	ann->is_trained = true;

	return 0.0f;
}

float ANN::BackPropTrainingIteration(std::shared_ptr<ANN::ANeuralNetwork> ann, std::vector<float>& input, std::vector<float>& output, float speed)
{
	ANeuralNetworkHeir obj;
	std::vector<size_t> config;
	config = ann->GetConfiguration();

	std::vector<float> Out;
	obj.ValueOfActivFuncForEveryNeuron.clear();
	obj.AverageErrForEveryNeuron.clear();
	obj.ValueOfActivFuncForEveryNeuron.resize(config.size());
	obj.AverageErrForEveryNeuron.resize(config.size());

	for (unsigned int layer_index = 0; layer_index < config.size(); layer_index++) {
		obj.ValueOfActivFuncForEveryNeuron[layer_index].resize(config[layer_index]);
		obj.AverageErrForEveryNeuron[layer_index].resize(config[layer_index]);
	}

	for (unsigned int from_index = 0; from_index < input.size(); from_index++) {

		obj.ValueOfActivFuncForEveryNeuron[0][from_index] = input[from_index];
	}

	for (unsigned int layer_index = 0; layer_index < config.size() - 1; layer_index++) {
		for (unsigned int from_index = 0; from_index < ann->weights[layer_index].size(); from_index++) {
			for (unsigned int to_index = 0; to_index < ann->weights[layer_index][from_index].size(); to_index++) {

				obj.ValueOfActivFuncForEveryNeuron[layer_index + 1][to_index] += obj.ValueOfActivFuncForEveryNeuron[layer_index][from_index]
					* ann->weights[layer_index][from_index][to_index];
			}
		}

		for (int i = 0; i < obj.ValueOfActivFuncForEveryNeuron[layer_index + 1].size(); i++) {
			obj.ValueOfActivFuncForEveryNeuron[layer_index + 1][i] = ann->Activation(obj.ValueOfActivFuncForEveryNeuron[layer_index + 1][i]);
		}
	}

	for (int i = 0; i < config[config.size() - 1]; i++) {
		Out.push_back(obj.ValueOfActivFuncForEveryNeuron[config.size() - 1][i]);
	}

	std::vector<float> predictResult;
	predictResult = Out;
	float error = 0;

	for (int i = 0; i < predictResult.size(); i++) {
		error += (predictResult[i] - output[i]) * (predictResult[i] - output[i]);
	}


	for (int layer_index = config.size() - 2; layer_index >= 0; layer_index--) {
		for (unsigned int from_index = 0; from_index < ann->weights[layer_index].size(); from_index++) {

			for (unsigned int to_index = 0; to_index < ann->weights[layer_index][from_index].size(); to_index++) {
				if (layer_index == config.size() - 2) {
					obj.AverageErrForEveryNeuron[layer_index + 1][to_index] = output[to_index] - predictResult[to_index];
					obj.AverageErrForEveryNeuron[layer_index][from_index] += obj.AverageErrForEveryNeuron[layer_index + 1][to_index]
						* ann->weights[layer_index][from_index][to_index];
				}
				else
					obj.AverageErrForEveryNeuron[layer_index][from_index] += obj.AverageErrForEveryNeuron[layer_index + 1][to_index]
					* ann->weights[layer_index][from_index][to_index];
			}
		}
	}

	for (unsigned int layer_index = 0; layer_index < config.size() - 1; layer_index++) {
		for (unsigned int from_index = 0; from_index < ann->weights[layer_index].size(); from_index++) {
			for (unsigned int to_index = 0; to_index < ann->weights[layer_index][from_index].size(); to_index++) {
				if (layer_index == 0) {
					ann->weights[layer_index][from_index][to_index] = ann->weights[layer_index][from_index][to_index] + speed
						* obj.AverageErrForEveryNeuron[layer_index + 1][to_index] * input[from_index]
						* ann->ActivationDerivative(obj.ValueOfActivFuncForEveryNeuron[layer_index + 1][to_index]);
				}
				else {
					ann->weights[layer_index][from_index][to_index] = ann->weights[layer_index][from_index][to_index] + speed
						* obj.AverageErrForEveryNeuron[layer_index + 1][to_index]
						* obj.ValueOfActivFuncForEveryNeuron[layer_index][from_index]
						* ann->ActivationDerivative(obj.ValueOfActivFuncForEveryNeuron[layer_index + 1][to_index]);
				}
			}
		}
	}

	Out.clear();
	config.clear();
	predictResult.clear();

	return sqrt(error);
}
#pragma once
#define ANNDLL_EXPORTS
#include "ANN.h"

using namespace std;

namespace ANN
{
	class ANeuralNetworkHeir : public ANN::ANeuralNetwork
	{
	public:

		ANeuralNetworkHeir();
		ANeuralNetworkHeir(vector<size_t>& config, ActivationType activation_t, float scl);
		~ANeuralNetworkHeir();

		vector<vector<float>> ValueOfActivFuncForEveryNeuron;
		vector<vector<float>> AverageErrForEveryNeuron;

		//��������� ������ �������� ����
		string GetType();

		//��������������� ������ �� ���������� �����
		vector<float> Predict(vector<float>& input);
	};
}




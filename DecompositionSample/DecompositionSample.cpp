#include <iostream>
#include "Visualisation.h"
#include "Heirs.h"

using namespace std;
using namespace fe;
using namespace cv;

int main()
{
	setlocale(LC_ALL, "ru");

	int PolynomOrder;
	int DiameterPix = 100;

	cout << "   ������� ������������ ������� ���������" << endl << "   ";
	cin >> PolynomOrder;

	//������� ������ ��� ������ � ����������
	auto ObjPoly = CreatePolynomialManager();
	cout << "   ������� ������ ��� ������ � ����������" << endl;

	//���������� ����� ����������� ���������
	ObjPoly->InitBasis(PolynomOrder, DiameterPix);
	cout << "   ������������� ����� ����������� ���������" << endl;

	//�������� �����
	OrthoBasis pln = ObjPoly->GetBasis();
	cout << "   �������� �����" << endl;

	//���������� ������
	ShowPolynomials("����������� ������", pln);
	cout << "   ���������� �����" << endl;

	//������� ���������� � ������
	string infoBasis = ObjPoly->GetType();
	cout << "\n" << "   ���������� � ������: " << "\n" << infoBasis << "\n\n";

	//��������� ���� � ������������
	Mat image = imread("image.png", IMREAD_GRAYSCALE);
	cout << "   ������� ���� � ������������" << endl;

	//������� ���������� ������� ��������
	auto an = CreateBlobProcessor();
	cout << "   ������� ���������� ������� ��������" << endl;
	
	//������� ������� �������
	vector<Mat> smezhObl; 
	threshold(image, image, 127, 255, THRESH_BINARY_INV);
	smezhObl = an->DetectBlobs(image);
	cout << "   ����� ������� �������" << endl;

	//�������� � ������� ��������
	vector<Mat> normalize = an->NormalizeBlobs(smezhObl, DiameterPix);
	cout << "   ������� � ������� ��������" << endl;

	//������� ���������� � ���� �����������
	string infoBlob = an->GetType();
	cout << "\n\n" << "   ������ ���������� � ���� �����������: " << "\n" << infoBlob << "\n\n";
	
	for (int i = 0; i < normalize.size(); i++)
	{
		ComplexMoments decomp;
		//���������� ��� ����� �� �������, ����������� �� � ���
		decomp = ObjPoly->Decompose(normalize[i]);
		if (i == normalize.size() - 1) cout << "   ��������� ��� ����� �� �������, ����������� �� � ���" << endl;
		
		//��������������� ����������� ����
		Mat vosstImage = ObjPoly->Recovery(decomp);
		if (i == normalize.size() - 1) cout << "   ������������ ����������� ����" << endl;
	
		//���������� ��������� ��������������
		ShowBlobDecomposition("��������������� �����������", normalize[i], vosstImage);
		if (i == normalize.size() - 1) cout << "   ���������� ��������� ��������������" << endl;

		waitKey();
	}

	return 0;
}